<?php # Script 12.13 - loggedin.php #3
// The customer is redirected here from login.php.

session_start(); // Start the session.

// If no session value is present, redirect the customer:
// Also validate the HTTP_USER_AGENT!
if (!isset($_SESSION['agent']) OR ($_SESSION['agent'] != md5($_SERVER['HTTP_USER_AGENT']) )) {

	// Need the functions:
	require ('includes/login_functions.inc.php');
	redirect_customer();	

}

// Set the page title and include the HTML header:
$page_title = 'Logged In!';
include ('includes/header.html');
?>
  <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p>You are now logged in, <?php echo $_SESSION['first_name'] . " " . $_SESSION['last_name']; ?>!</p>
  </div>
  <div>
    <a href="browse_songs.php" class="btn btn-success"><span class="glyphicon glyphicon-music"></span> Browse Songs</a>
    <a href="logout.php" class="btn btn-primary"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
  </div>
<?php

include ('includes/footer.html');
?>