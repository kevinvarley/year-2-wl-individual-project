<?php
  session_start();

  if (!isset($_SESSION['customer_id'])) {
    header('Location: login.php');
    exit();
  }

  $page_title = 'My Tunes';
  require_once('includes/header.html');

  require("../mysqli_connect.php");

  $q = "SELECT songs.song_id, artists.artist_name, songs.song_name, DATE_FORMAT(songs.length, '%i:%s') AS song_length, songs.image_name FROM `orders`, `order_contents`, `songs`, `artists` WHERE orders.order_id=order_contents.order_id AND order_contents.song_id=songs.song_id AND songs.artist_id=artists.artist_id AND orders.customer_id={$_SESSION['customer_id']}";
  $r = mysqli_query ($dbc, $q);
  echo "<p><strong>Number of tunes:</strong> " . mysqli_num_rows($r) . "</p>";
?>
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Cover Art</th>
        <th>Artist</th>
        <th>Track</th>
        <th>Length</th>
      </tr>
    </thead>
    <tbody>
<?php
  while ($row = mysqli_fetch_array ($r, MYSQLI_ASSOC)) {
    echo "<tr><td>";
    if ($image = @getimagesize ("../uploads/covers/{$row['song_id']}")) {
      echo "<img src=\"show_image.php?image={$row['song_id']}&name=" . urlencode($row['image_name']) . "\" $image[3] alt=\"{$row['song_name']}\" class=\"coverart thumbnail\">";  
    } else {
      echo "No image available."; 
    }
    echo "</td><td>{$row['artist_name']}</td><td>{$row['song_name']}</td><td>{$row['song_length']}</td></tr>";
  }
  echo "</tbody></table>";
?>
<?php
  require_once('includes/footer.html');
?>