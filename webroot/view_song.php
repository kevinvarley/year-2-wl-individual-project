<?php # Script 19.7 - view_song.php
// This page displays the details for a particular song.

$row = FALSE; // Assume nothing!

if (isset($_GET['pid']) && filter_var($_GET['pid'], FILTER_VALIDATE_INT, array('min_range' => 1)) ) { // Make sure there's a song ID!

	$pid = $_GET['pid'];
	
	// Get the song info:
	require ('../mysqli_connect.php'); // Connect to the database.
	$q = "SELECT artist_name AS artist, song_name, price, DATE_FORMAT(length, '%i:%s') AS length, image_name FROM artists, songs WHERE artists.artist_id=songs.artist_id AND songs.song_id=$pid";
	$r = mysqli_query ($dbc, $q);
	if (mysqli_num_rows($r) == 1) { // Good to go!

		// Fetch the information:
		$row = mysqli_fetch_array ($r, MYSQLI_ASSOC);
	
		// Start the HTML page:
		$page_title = $row['song_name'];
		include ('includes/header.html');
	
		// Display a header:
		echo "<div align=\"center\">
		<b>{$row['song_name']}</b> by 
		{$row['artist']}<br />";

		// song the length or a default message:
		echo (is_null($row['length'])) ? '(No length available)' : $row['length'];

		echo "<br />&pound;{$row['price']} 
		<a href=\"add_cart.php?pid=$pid\">Add to Cart</a>
		</div><br />";
	
		// Get the image information and display the image:
		if ($image = @getimagesize ("../uploads/covers/$pid")) {
			echo "<div align=\"center\"><img src=\"show_image.php?image=$pid&name=" . urlencode($row['image_name']) . "\" $image[3] alt=\"{$row['song_name']}\" class=\"img-responsive\"></div>\n";	
		} else {
			echo "<div align=\"center\">No image available.</div>\n"; 
		}
	
	} // End of the mysqli_num_rows() IF.
	
	mysqli_close($dbc);

} // End of $_GET['pid'] IF.

if (!$row) { // Show an error message.
	$page_title = 'Error';
	include ('includes/header.html');
	echo '<div align="center">This page has been accessed in error!</div>';
}

// Complete the page:
include ('includes/footer.html');
?>