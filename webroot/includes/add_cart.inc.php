<?php
function add_to_cart($sid) {
	if (isset ($sid) && filter_var($sid, FILTER_VALIDATE_INT, array('min_range' => 1))  ) {
		if (isset($_SESSION['cart'][$sid])) {
			return "duplicate";
		}
		else {
			require ('../mysqli_connect.php');
			$q = "SELECT price FROM songs WHERE song_id=$sid";
			$r = mysqli_query ($dbc, $q);		
			if (mysqli_num_rows($r) == 1) {
				list($price) = mysqli_fetch_array ($r, MYSQLI_NUM);
				$_SESSION['cart'][$sid] = array ('quantity' => 1, 'price' => $price);
				return "success";
			}
			else {
				return "invalid";
			}
			
			mysqli_close($dbc);

		}
	}
	else {
		return "noid";
	}
}
?>