<?php # Script 12.1 - login_page.inc.php
// This page prints any errors associated with logging in
// and it creates the entire login page, including the form.

// Include the header:
include ('../includes/adminheader.html');


// Print any error messages, if they exist:
if (isset($errors) && !empty($errors)) {
	echo '<h1>Error!</h1>
	<p class="error">The following error(s) occurred:<br />';
	foreach ($errors as $msg) {
		echo " - $msg<br />\n";
	}
	echo '</p><p>Please try again.</p>';
}
?>

<?php
  if(isset($_SESSION['postlogin'])) {
    unset($_SESSION['postlogin']);
?>
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong>Successfully logged in!</strong>
    </div>
<?php
  }
?>

<?php
// Display the form:
if(!isset($_SESSION['admin_id'])) {
?>

<form action="login.php" method="post" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Login</legend>

<div class="alert alert-info"><p class="text-bold">User login details</p><p><strong>Email Address:</strong> test@user.co.uk</p><p><strong>Password:</strong> password</p></div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email Address:</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="john.smith@example.com" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="pass">Password:</label>
  <div class="col-md-4">
    <input id="pass" name="pass" type="password" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-log-in"></span> Login</button>
  </div>
</div>

</fieldset>
</form>
<?php
	}
  else {
?>
    <h1>EZTunes Backend</h1>
    <div class="alert alert-info">
      <p>Please use the "Admin Menu" above to perform administration tasks...</p>
    </div>
<?php
  }
?>

<?php include ('../includes/adminfooter.html'); ?>