<?php # Script 9.6 - view_customers.php #2
// This script retrieves all the records from the customers table.
session_start();

if (!isset($_SESSION['admin_id'])) {
	header('Location: index.php');
}

require ('../../mysqli_connect.php'); // Connect to the db.

$customersPerPage = 1;

$paginationQuery = "SELECT COUNT(customers.customer_id) AS number_of_customers FROM customers";
$paginationResponse = mysqli_query($dbc, $paginationQuery);
if(mysqli_num_rows($paginationResponse) === 1) {
	$row = mysqli_fetch_assoc($paginationResponse);
	$numberOfPages = ceil($row['number_of_customers'] / $customersPerPage);
	$numberOfCustomers = $row['number_of_customers'];
}

if(isset($_GET['page']) && ctype_digit($_GET['page']) && $_GET['page'] <= $numberOfPages) {
	$currentPage = $_GET['page'];
}
else {
	$currentPage = 1;
}

$offset = (($currentPage - 1) * $customersPerPage);

$page_title = 'View the Current customers';
include ('../includes/adminheader.html');

echo "<div style=\"margin-bottom: 1rem;\"><a href=\"index.php\" class=\"btn btn-success\"><span class=\"fa fa-arrow-circle-left\"></span> Back To Admin Home</a></div>";

// Page header:
echo '<h1>Registered customers</h1>';
		
// Make the query:
$q = "SELECT customer_id, CONCAT(last_name, ', ', first_name) AS name, DATE_FORMAT(registration_date, '%M %d, %Y') AS dr FROM customers ORDER BY registration_date ASC LIMIT $offset,$customersPerPage";		
$r = @mysqli_query ($dbc, $q); // Run the query.

// Count the number of returned rows:
$num = mysqli_num_rows($r);

if ($num > 0) { // If it ran OK, display the records.

	// Print how many customers there are:
	echo "<p>There are currently $numberOfCustomers registered customers.</p>\n";

	// Table header.
	echo '<table class="table table-striped">
	<thead><tr><th>Name</th><th>Date Registered</th><th>Edit</th><th>Delete</th></tr></thead>
';
	
	// Fetch and print all the records:
	while ($row = mysqli_fetch_array($r, MYSQLI_ASSOC)) {
		echo '<tr><td align="left">' . $row['name'] . '</td><td align="left">' . $row['dr'] . '</td><td><a href="edit_customer.php?id=' . $row['customer_id'] . '" class="btn btn-info"><span class="glyphicon glyphicon-edit"></span></a></td><td><a href="delete_customer.php?id=' . $row['customer_id']  . '" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span></td></tr>
		';
	}

	echo '</table>'; // Close the table.
	
	mysqli_free_result ($r); // Free up the resources.	

} else { // If no records were returned.

	echo '<p class="error">There are currently no registered customers.</p>';

}
?>
	<ul class="pagination">
		<?php
			if(($currentPage - 1)>0) {
				echo "<li><a href=\"{$_SERVER['PHP_SELF']}?page=" . ($currentPage-1) . "\">&laquo;</a>";
			}
			else {
				echo "<li class=\"disabled\"><a href=\"#\">&laquo;</a>";
			}
		
			for($i=1; $i<=$numberOfPages; $i++) {
				echo "<li";
				if($i == $currentPage) {
			    echo " class=\"active\"";
			  }
			  echo "><a href=\"{$_SERVER['PHP_SELF']}?page={$i}\">$i</a></li>";
			}

			if($currentPage < $numberOfPages) {
				echo "<li><a href=\"{$_SERVER['PHP_SELF']}?page=" . ($currentPage+1) . "\">&raquo;</a>";
			}
			else {
				echo "<li class=\"disabled\"><a href=\"#\">&raquo;</a>";
			}
		?>
	</ul>
<?php

mysqli_close($dbc); // Close the database connection.

include ('../includes/adminfooter.html');
?>