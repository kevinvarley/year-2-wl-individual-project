<?php # Script 19.2 - add_song.php
session_start();

if (!isset($_SESSION['admin_id'])) {
	header('Location: index.php');
}

$page_title = 'Add a song';
include ('../includes/adminheader.html');
// This page allows the administrator to add a song (product).

require ('../../mysqli_connect.php');

if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Handle the form.
	
	// Validate the incoming data...
	$errors = array();

	// Check for a song name:
	if (!empty($_POST['song_name'])) {
		$pn = trim($_POST['song_name']);
	} else {
		$errors[] = 'Please enter the song\'s name!';
	}
	
	// Check for an image:
	if (is_uploaded_file ($_FILES['image']['tmp_name'])) {

		// Create a temporary file name:
		$temp = '../../uploads/covers/' . md5($_FILES['image']['name']);
	
		// Move the file over:
		if (move_uploaded_file($_FILES['image']['tmp_name'], $temp)) {

			chmod($temp, 0774);

			echo '<p>The file has been uploaded!</p>';

	
			// Set the $i variable to the image's name:
			$i = $_FILES['image']['name'];
	
		} else { // Couldn't move the file over.
			$errors[] = 'The file could not be moved.';
			$temp = $_FILES['image']['tmp_name'];
		}

	} else { // No uploaded file.
		$errors[] = 'No file was uploaded.';
		$temp = NULL;
	}
	
	// Check for a length (not required):
	$s = (!empty($_POST['length'])) ? trim($_POST['length']) : NULL;

	// Check for a price:
	if (is_numeric($_POST['price']) && ($_POST['price'] > 0)) {
		$p = (float) $_POST['price'];
	} else {
		$errors[] = 'Please enter the song\'s price!';
	}
	
	// Validate the artist...
	if ( isset($_POST['artist']) && filter_var($_POST['artist'], FILTER_VALIDATE_INT, array('min_range' => 1))  ) {
		$a = $_POST['artist'];
	} else { // No artist selected.
		$errors[] = 'Please select the song\'s artist!';
	}
	
	if (empty($errors)) { // If everything's OK.

		// Add the song to the database:
		$q = 'INSERT INTO songs (artist_id, song_name, price, length, image_name) VALUES (?, ?, ?, ?, ?)';
		$stmt = mysqli_prepare($dbc, $q);
		mysqli_stmt_bind_param($stmt, 'isdss', $a, $pn, $p, str_replace(':', '', $s), $i);
		mysqli_stmt_execute($stmt);
		
		// Check the results...
		if (mysqli_stmt_affected_rows($stmt) == 1) {

			echo "<div class=\"alert alert-success alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button> <p>Song successfully added...</p></div>";
	
			// Rename the image:
			$id = mysqli_stmt_insert_id($stmt); // Get the song ID.
			rename ($temp, "../../uploads/covers/$id");
	
			// Clear $_POST:
			$_POST = array();
	
		} else { // Error!
			echo '<p style="font-weight: bold; color: #C00">Your submission could not be processed due to a system error.</p>'; 
		}
		
		mysqli_stmt_close($stmt);
		
	} // End of $errors IF.
	
	// Delete the uploaded file if it still exists:
	if ( isset($temp) && file_exists ($temp) && is_file($temp) ) {
		unlink ($temp);
	}
	
} // End of the submission IF.

// Check for any errors and song them:
if ( !empty($errors) && is_array($errors) ) {
	echo '<h1>Error!</h1>
	<p style="font-weight: bold; color: #C00">The following error(s) occurred:<br />';
	foreach ($errors as $msg) {
		echo " - $msg<br />\n";
	}
	echo 'Please reselect the song image and try again.</p>';
}

echo "<div style=\"margin-bottom: 1rem;\"><a href=\"index.php\" class=\"btn btn-success\"><span class=\"fa fa-arrow-circle-left\"></span> Back To Admin Home</a></div>";

// Display the form...
?>

<form enctype="multipart/form-data" action="add_song.php" method="post" class="form-horizontal">
<input type="hidden" name="MAX_FILE_SIZE" value="524288" />
<fieldset>

<!-- Form Name -->
<legend>Add a song</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="song_name">Song Name</label>  
  <div class="col-md-4">
  <input id="song_name" name="song_name" type="text" placeholder="Red Lights" class="form-control input-md" required="">
    
  </div>
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="image">Cover Art:</label>
  <div class="col-md-4">
    <input id="image" name="image" class="input-file" type="file">
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="artist">Artist:</label>
  <div class="col-md-4">
    <select id="artist" name="artist" class="form-control">
	    <option>Select One</option>
			<?php // Retrieve all the artists and add to the pull-down menu.
			$q = "SELECT artist_id, artist_name FROM artists ORDER BY artist_name ASC";		
			$r = mysqli_query ($dbc, $q);
			if (mysqli_num_rows($r) > 0) {
				while ($row = mysqli_fetch_array ($r, MYSQLI_NUM)) {
					echo "<option value=\"$row[0]\"";
					// Check for stickyness:
					if (isset($_POST['artist']) && ($_POST['artist'] == $row[0]) ) echo ' selected="selected"';
					echo ">$row[1]</option>\n";
				}
			} else {
				echo '<option>Please add a new artist first.</option>';
			}
			mysqli_close($dbc); // Close the database connection.
			?>
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="price">Price:</label>  
  <div class="col-md-4">
  <input id="price" name="price" type="text" placeholder="0.99" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="length">Length (e.g. 01:59):</label>  
  <div class="col-md-4">
  <input id="length" name="length" type="text" placeholder="03:23" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Add Song</button>
  </div>
</div>

</fieldset>
</form>

<?php include("../includes/adminfooter.html"); ?>