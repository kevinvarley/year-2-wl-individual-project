<?php
session_start();

if (!isset($_SESSION['admin_id'])) {
	header('Location: index.php');
}

$page_title = 'Edit a song';
include ('../includes/adminheader.html');
// This page allows the administrator to edit a song (product).

echo "<div style=\"margin-bottom: 1rem;\"><a href=\"browse_songs.php\" class=\"btn btn-success\"><span class=\"fa fa-arrow-circle-left\"></span> Back To Admin Song Browse</a></div>";

require ('../../mysqli_connect.php');

if(isset($_POST['update-song'])) {
  $q = "SELECT songs.*, DATE_FORMAT(length, '%i:%s') AS song_length, artists.artist_name FROM songs, artists WHERE songs.song_id={$_POST['song_id']} AND songs.artist_id=artists.artist_id";
  $r = mysqli_query($dbc, $q);
  if(mysqli_num_rows($r) === 1) {
    $row = mysqli_fetch_assoc($r);
  }

  $fieldsToUpdate = Array();

  // check if song name needs updating
  if($row['song_name'] !== $_POST['song_name']) {
    $fieldsToUpdate[] = "song_name";
  }

  // check if song name needs updating
  if($row['artist_id'] !== $_POST['artist']) {
    $fieldsToUpdate[] = "artist_id";
  }

  // check if cover art image needs updating
  if (is_uploaded_file($_FILES['image']['tmp_name'])) {
    $fieldsToUpdate[] = "image_name";
  }

  // check if song name needs updating
  if($row['price'] !== $_POST['price']) {
    $fieldsToUpdate[] = "price";
  }

  // check if song name needs updating
  if($row['song_length'] !== $_POST['length']) {
    $fieldsToUpdate[] = "length";
  }

  if(sizeof($fieldsToUpdate) > 0) {
    $q = "UPDATE songs SET ";
    for($i = 0; $i <= sizeof($fieldsToUpdate)-1; $i++) {
      if($fieldsToUpdate[$i] == "image_name") {
        $_POST['image_name'] = $_FILES['image']['name'];
      }
      $q .= $fieldsToUpdate[$i] . "='" . $_POST[$fieldsToUpdate[$i]] . "'";
      if(($i+1) < sizeof($fieldsToUpdate)) {
        $q .= ", ";
      }
      else {
        $q .= " ";
      }
    }
    $q .= " WHERE songs.song_id='{$_POST['song_id']}' LIMIT 1";
    $r = @mysqli_query ($dbc, $q);
    if (mysqli_affected_rows($dbc) == 1) { // If it ran OK.
      echo "<div class=\"alert alert-success\"><p>Successfully updated song!</p></div>";
    }
    else {
      echo $q;
      echo "<div class=\"alert alert-danger\"><p><strong>Error:</strong> Failed to update song!</p></div>";
    }
  }

  // check if covert art needs updating
  if (is_uploaded_file ($_FILES['image']['tmp_name'])) {
    // check if a cover art image already exists for this song id
    if(is_readable(realpath('../../uploads/covers') . "/{$_POST['song_id']}")) {
      // delete the covert art image for this song id
      // if it returns true, the image was deleted and we show a message
      if(!unlink(realpath('../../uploads/covers') . "/{$_POST['song_id']}") === true) {
        die("<p>Failed to delete cover art for this song. Please manually remove \"" . realpath('../../uploads/covers') . "/{$_POST['song_id']}\" and fix the permissions on this web server.</p>");
      }
    }

    // Create a temporary file name:
    $temp = '../../uploads/covers/' . $_POST['song_id'];
  
    // Move the file over:
    if (move_uploaded_file($_FILES['image']['tmp_name'], $temp)) {

      chmod($temp, 0774);

      echo "<div class=\"alert alert-success\">The cover art has been updated!</p></div>";

  
      // Set the $i variable to the image's name:
      $i = $_FILES['image']['name'];
  
    } else { // Couldn't move the file over.
      $errors[] = 'The file could not be moved.';
      $temp = $_FILES['image']['tmp_name'];
    }
  }
  else { // No uploaded file.
    $errors[] = 'No file was uploaded.';
    $temp = NULL;
  }

}

if(isset($_GET['id']) && ctype_digit($_GET['id']) && !isset($_POST['update-song'])) {
  $songId = $_GET['id'];
	$q = "SELECT songs.*, DATE_FORMAT(length, '%i:%s') AS song_length, artists.artist_name FROM songs, artists WHERE songs.song_id={$songId} AND songs.artist_id=artists.artist_id";
	$r = mysqli_query($dbc, $q);
  if(mysqli_num_rows($r) === 1) {
    $row = mysqli_fetch_assoc($r);
?>
    <form enctype="multipart/form-data" action="edit_song.php" method="post" class="form-horizontal">
      <input type="hidden" name="MAX_FILE_SIZE" value="524288">
      <input type="hidden" name="song_id" value="<?php echo $row['song_id']; ?>">
      <fieldset>

      <!-- Form Name -->
      <legend>Edit a song</legend>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="song_name">Song Name</label>  
        <div class="col-md-4">
        <input id="song_name" name="song_name" type="text" placeholder="Red Lights" class="form-control input-md" required="" value="<?php echo $row['song_name']; ?>">
          
        </div>
      </div>

      <!-- File Button --> 
      <div class="form-group">
        <label class="col-md-4 control-label" for="image">Cover Art:</label>
        <div class="col-md-4">
          <?php
            if ($image = @getimagesize ("../../uploads/covers/$row[song_id]")) {
              echo "<img src=\"show_image.php?image=$row[song_id]&name=" . urlencode($row['image_name']) . "\" $image[3] alt=\"{$row['song_name']}\" class=\"coverart thumbnail\">";  
            } else {
              echo "No image available."; 
            }
          ?>
          <input id="image" name="image" class="input-file" type="file">
        </div>
      </div>

      <!-- Select Basic -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="artist">Artist:</label>
        <div class="col-md-4">
          <select id="artist" name="artist" class="form-control">
            <option>Select One</option>
            <?php // Retrieve all the artists and add to the pull-down menu.
            $q2 = "SELECT artist_id, artist_name FROM artists ORDER BY artist_name ASC";   
            $r2 = mysqli_query ($dbc, $q2);
            if (mysqli_num_rows($r2) > 0) {
              while ($row2 = mysqli_fetch_array ($r2, MYSQLI_NUM)) {
                echo "<option value=\"$row2[0]\"";
                // Check for stickyness:
                if ( $row2[0] === $row['artist_id'] ) echo ' selected="selected"';
                echo ">$row2[1]</option>\n";
              }
            } else {
              echo '<option>Please add a new artist first.</option>';
            }
            mysqli_close($dbc); // Close the database connection.
            ?>
          </select>
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="price">Price:</label>  
        <div class="col-md-4">
        <input id="price" name="price" type="text" placeholder="0.99" class="form-control input-md" required="" value="<?php echo $row['price']; ?>">
          
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="length">Length:</label>  
        <div class="col-md-4">
        <input id="length" name="length" type="text" placeholder="03:23" class="form-control input-md" required="" value="<?php echo $row['song_length']; ?>">
          
        </div>
      </div>

      <!-- Button -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="submit"></label>
        <div class="col-md-4">
          <button id="submit" name="update-song" class="btn btn-primary">Update Song</button>
        </div>
      </div>

      </fieldset>
      </form>
<?php
  }
}
elseif(!isset($_POST['update-song'])) {
  echo "<div class=\"alert alert-danger\"><p><strong>Error:</strong> this page has been accessed in error! <a href=\"../index.php\">Return to EZTunes home</a>.</p></div>";
}
?>

<?php include("../includes/adminfooter.html"); ?>