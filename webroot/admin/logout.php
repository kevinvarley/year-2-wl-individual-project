<?php # Script 12.11 - logout.php #2
// This page lets the customer logout.
// This version uses sessions.

session_start(); // Access the existing session.

// If no session variable exists, redirect the customer:
if (!isset($_SESSION['admin_id'])) {

	// Need the functions:
	require ('../includes/login_functions.inc.php');
	redirect_customer();
	
} else { // Cancel the session:

	$_SESSION = array(); // Clear the variables.
	session_destroy(); // Destroy the session itself.
	setcookie ('PHPSESSID', '', time()-3600, '/', '', 0, 0); // Destroy the cookie.

}

// Set the page title and include the HTML header:
$page_title = 'Logged Out!';
include ('../includes/adminheader.html');
?>
  <div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Successfully logged out...</strong>
  </div>

  <div>
    <a href="../index.php" class="btn btn-primary"><span class="glyphicon glyphicon-home"></span> Return to EZTunes home</a>
    <a href="index.php" class="btn btn-success"><span class="glyphicon glyphicon-log-in"></span> Login again to EZTunes admin</a>
  </div>
<?php

include ('../includes/adminfooter.html');
?>