<?php # Script 10.3 - edit_customer.php
// This page is for editing a customer record.
// This page is accessed through view_customers.php.
session_start();

if (!isset($_SESSION['admin_id'])) {
	header('Location: index.php');
}

$page_title = 'Edit a customer';
include ('../includes/adminheader.html');

// Check for a valid customer ID, through GET or POST:
if ( (isset($_GET['id'])) && (is_numeric($_GET['id'])) ) { // From view_customers.php
	$id = $_GET['id'];
} elseif ( (isset($_POST['id'])) && (is_numeric($_POST['id'])) ) { // Form submission.
	$id = $_POST['id'];
} else { // No valid ID, kill the script.
	echo "<div class=\"alert alert-warning\"><p>This page has been accessed in error. <a href=\"../index.php\">Return to EZTunes home</a>.</p></div>";
	include ('../includes/adminfooter.html'); 
	exit();
}

require ('../../mysqli_connect.php'); 

// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$errors = array();
	
	// Check for a first name:
	if (empty($_POST['first_name'])) {
		$errors[] = 'You forgot to enter your first name.';
	} else {
		$fn = mysqli_real_escape_string($dbc, trim($_POST['first_name']));
	}
	
	// Check for a last name:
	if (empty($_POST['last_name'])) {
		$errors[] = 'You forgot to enter your last name.';
	} else {
		$ln = mysqli_real_escape_string($dbc, trim($_POST['last_name']));
	}

	// Check for an email address:
	if (empty($_POST['email'])) {
		$errors[] = 'You forgot to enter your email address.';
	} else {
		$e = mysqli_real_escape_string($dbc, trim($_POST['email']));
	}
	
	if (empty($errors)) { // If everything's OK.
	
		//  Test for unique email address:
		$q = "SELECT customer_id FROM customers WHERE email='$e' AND customer_id != $id";
		$r = @mysqli_query($dbc, $q);
		if (mysqli_num_rows($r) == 0) {

			// Make the query:
			$q = "UPDATE customers SET first_name='$fn', last_name='$ln', email='$e' WHERE customer_id=$id LIMIT 1";
			$r = @mysqli_query ($dbc, $q);
			if (mysqli_affected_rows($dbc) == 1) { // If it ran OK.

				// Print a message:
				echo "<div class=\"alert alert-success\"><p>The customer has been edited.</p></div>";
				
			} else { // If it did not run OK.
				echo "<div class=\"alert alert-danger\"><p>The customer could not be edited due to a system error. We apologize for any inconvenience.</p></div>"; // Public message.
				echo "<div class=\"alert alert-danger\"><p>" . mysqli_error($dbc) . "<br />Query: " . $q . "</p></div>"; // Debugging message.
			}
				
		} else { // Already registered.
			echo "<div class=\"alert alert-danger\"><p>The email address has already been registered.</p></div>";
		}
		
	} else { // Report the errors.

		echo "<div class=\"alert alert-danger\"><p>The following error(s) occurred:<br /></div>";
		foreach ($errors as $msg) { // Print each error.
			echo " - $msg<br />\n";
		}
		echo "</p><p>Please try again.</p></div>";
	
	} // End of if (empty($errors)) IF.

} // End of submit conditional.

// Always show the form...

// Retrieve the customer's information:
$q = "SELECT first_name, last_name, email FROM customers WHERE customer_id=$id";		
$r = @mysqli_query ($dbc, $q);

if (mysqli_num_rows($r) == 1) { // Valid customer ID, show the form.

	// Get the customer's information:
	$row = mysqli_fetch_array ($r, MYSQLI_NUM);
?>
	<form action="edit_customer.php" method="post" class="form-horizontal">
		<input type="hidden" name="id" value="<?php echo $id; ?>" />
		<fieldset>

		<!-- Form Name -->
		<legend>Edit Customer</legend>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="first_name">First Name</label>  
		  <div class="col-md-4">
		  <input id="first_name" name="first_name" type="text" placeholder="John" class="form-control input-md" size="15" maxlength="15" required="" value="<?php echo $row[0]; ?>">
		    
		  </div>
		</div>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="last_name">Last Name</label>  
		  <div class="col-md-4">
		  <input id="last_name" name="last_name" type="text" placeholder="Smith" class="form-control input-md" size="15" maxlength="30" required="" value="<?php echo $row[1]; ?>">
		    
		  </div>
		</div>

		<!-- Text input-->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="email">Email Address</label>  
		  <div class="col-md-4">
		  <input id="email" name="email" type="email" placeholder="j.smith@example.com" class="form-control input-md" size="20" maxlength="60" required="" value="<?php echo $row[2]; ?>">
		    
		  </div>
		</div>

		<!-- Button -->
		<div class="form-group">
		  <label class="col-md-4 control-label" for="submit"></label>
		  <div class="col-md-4">
		    <button id="submit" name="submit" class="btn btn-primary">Update Customer</button>
		  </div>
		</div>

		</fieldset>
	</form>
<?php

} else { // Not a valid customer ID.
	echo "<div class=\"alert alert-warning\"><p>This page has been accessed in error. <a href=\"../index.php\">Return to EZTunes home</a>.</p></div>";
}

mysqli_close($dbc);
		
include ('../includes/adminfooter.html');
?>