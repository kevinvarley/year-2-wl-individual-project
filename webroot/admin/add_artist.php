<?php # Script 19.1 - add_artist.php
// This script retrieves all the records from the customers table.
session_start();

if (!isset($_SESSION['admin_id'])) {
	header('Location: index.php');
}

$page_title = 'Add an artist';
include ('../includes/adminheader.html');
// This page allows the administrator to add an artist.

if ($_SERVER['REQUEST_METHOD'] == 'POST') { // Handle the form.
	
	// Validate the first and middle names (neither required):
	$an = (!empty($_POST['artist_name'])) ? trim($_POST['artist_name']) : NULL;

	if (!empty($_POST['artist_name'])) {
		
		$an = trim($_POST['artist_name']);
		
		// Add the artist to the database:
		require ('../../mysqli_connect.php');
		$q = 'INSERT INTO artists (artist_name) VALUES (?)';
		$stmt = mysqli_prepare($dbc, $q);
		mysqli_stmt_bind_param($stmt, 's', $an);
		mysqli_stmt_execute($stmt);
		
		// Check the results....
		if (mysqli_stmt_affected_rows($stmt) == 1) {
			echo "<div class=\"alert alert-success\"><p>The artist has been added.</p></div>";
			$_POST = array();
		} else { // Error!
			$error = 'The new artist could not be added to the database!';
		}
		
		// Close this prepared statement:
		mysqli_stmt_close($stmt);
		mysqli_close($dbc); // Close the database connection.
		
	} else { // No last name value.
		$error = 'Please enter the artist\'s name!';
	}
	
} // End of the submission IF.

// Check for an error and song it:
if (isset($error)) {
	echo '<h1>Error!</h1>
	<p style="font-weight: bold; color: #C00">' . $error . ' Please try again.</p>';
}

echo "<div style=\"margin-bottom: 1rem;\"><a href=\"index.php\" class=\"btn btn-success\"><span class=\"fa fa-arrow-circle-left\"></span> Back To Admin Home</a></div>";

// Display the form...
?>
<form action="add_artist.php" method="post" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Add Artist</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="artist_name">Artist Name</label>  
  <div class="col-md-4">
  <input id="artist_name" name="artist_name" type="text" placeholder="Daft Punk" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Add Artist</button>
  </div>
</div>

</fieldset>
</form>

<?php include('../includes/adminfooter.html'); ?>