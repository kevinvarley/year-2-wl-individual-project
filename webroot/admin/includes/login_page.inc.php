<?php # Script 12.1 - login_page.inc.php
// This page prints any errors associated with logging in
// and it creates the entire login page, including the form.

// Include the header:
$page_title = 'Login';
include ('../includes/adminheader.html');

// Print any error messages, if they exist:
if (isset($errors) && !empty($errors)) {
	echo "<div class=\"alert alert-danger\"><strong>Error:</strong>
	<p>The following error(s) occurred:</p>";
	foreach ($errors as $msg) {
		echo " - $msg<br />\n";
	}
	echo "</p><p>Please try again.</p></div>";
}

// Display the form:
?>

<form action="login.php" method="post" class="form-horizontal">
<?php
  if(isset($_GET['redirectto']) && ctype_alpha($_GET['redirectto'])) {
?>
    <input type="hidden" name="redirectto" value="<?php echo $_GET['redirectto']; ?>">
<?php
  }
  if(isset($_GET['pid']) && ctype_digit($_GET['pid'])) {
?>
    <input type="hidden" name="pid" value="<?php echo $_GET['pid']; ?>">
<?php
  }
?>
<fieldset>

<!-- Form Name -->
<legend>Login</legend>

<div class="alert alert-info"><p class="text-bold">User login details</p><p><strong>Email Address:</strong> test@user.co.uk</p><p><strong>Password:</strong> password</p></div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email Address:</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="john.smith@example.com" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="pass">Password:</label>
  <div class="col-md-4">
    <input id="pass" name="pass" type="password" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary btn-block">Login</button>
  </div>
</div>

<div class="form-group">
  <div class="col-md-4 col-md-offset-4">
  <a href="forgot_password.php">Forgotten your password?</a>
  </div>
</div>

</fieldset>
</form>

<?php include ('../includes/adminfooter.html'); ?>