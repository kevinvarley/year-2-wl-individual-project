<?php # Script 10.2 - delete_song.php
// This page is for deleting a song record.
// This page is accessed through view_songs.php.
session_start();

if (!isset($_SESSION['admin_id'])) {
	header('Location: index.php');
}

$page_title = 'Delete a song';
include ('../includes/adminheader.html');

echo "<div style=\"margin-bottom: 1rem;\"><a href=\"browse_songs.php\" class=\"btn btn-success\"><span class=\"fa fa-arrow-circle-left\"></span> Back To Admin Song Browse</a></div>";

echo '<h1>Delete a song</h1>';

// Check for a valid song ID, through GET or POST:
if ( (isset($_GET['id'])) && (is_numeric($_GET['id'])) ) { // From view_songs.php
	$id = $_GET['id'];
} elseif ( (isset($_POST['id'])) && (is_numeric($_POST['id'])) ) { // Form submission.
	$id = $_POST['id'];
} else { // No valid ID, kill the script.
	echo '<p class="error">This page has been accessed in error.</p>';
	include ('../includes/footer.html'); 
	exit();
}

require ('../../mysqli_connect.php');

// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	if ($_POST['sure'] == 'Yes') { // Delete the record.

		// Make the query:
		$q = "DELETE FROM songs WHERE song_id=$id LIMIT 1";		
		$r = @mysqli_query ($dbc, $q);
		if (mysqli_affected_rows($dbc) == 1) { // If it ran OK.

			// Print a message:
			echo '<p>The song has been deleted from the database.</p>';

			// check if a cover art image exists for this song id
			if(is_readable(realpath('../../uploads/covers') . "/$id")) {
				// delete the covert art image for this song id
				// if it returns true, the image was deleted and we show a message
				if(unlink(realpath('../../uploads/covers') . "/$id") === true) {
					echo "<p>Cover art was successfully deleted from the filesystem.</p>";
				}
				else {
					echo "<p>Failed to delete cover art for this song. Please manually remove \"" . realpath('../../uploads/covers') . "/$id\" and fix the permissions on this web server.</p>";
				}
			}	

		} else { // If the query did not run OK.
			echo '<p class="error">The song could not be deleted due to a system error.</p>'; // Public message.
			echo '<p>' . mysqli_error($dbc) . '<br />Query: ' . $q . '</p>'; // Debugging message.
		}
	
	} else { // No confirmation of deletion.
		echo '<p>The song has NOT been deleted.</p>';	
	}

} else { // Show the form.

	// Retrieve the song's information:
	$q = "SELECT artists.artist_name, songs.song_name FROM songs, artists WHERE song_id=$id AND songs.artist_id=artists.artist_id";
	$r = @mysqli_query ($dbc, $q);

	if (mysqli_num_rows($r) == 1) { // Valid song ID, show the form.

		// Get the song's information:
		$row = mysqli_fetch_array ($r, MYSQLI_NUM);
		
		// Display the record being deleted:
		echo "<h3>$row[0] - $row[1]</h3>
		Are you sure you want to delete this song?";

		// Create the form:
		echo '<form action="delete_song.php" method="post">
	<input type="radio" name="sure" value="Yes" /> Yes 
	<input type="radio" name="sure" value="No" checked="checked" /> No
	<input type="submit" name="submit" value="Submit" />
	<input type="hidden" name="id" value="' . $id . '" />
	</form>';
	
	} else { // Not a valid song ID.
		echo '<p class="error">This page has been accessed in error.</p>';
	}

} // End of the main submission conditional.

mysqli_close($dbc);
		
include ('../includes/adminfooter.html');
?>