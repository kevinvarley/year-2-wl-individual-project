<?php # Script 19.11 - checkout.php
// This page inserts the order information into the table.
// This page would come after the billing process.
// This page assumes that the billing process worked (the money has been taken).

// start the session
session_start();

// check if the custoer has logged in
// if they haven't then they shouldn't be accessing this page
if (!isset($_SESSION['customer_id'])) {
	header('Location: login.php');
	exit();
}

// check if the cart is set otherwise redirect the user to the browse_songs page
if(!isset($_SESSION['cart'])) {
	header('Location: browse_songs.php');
	exit();
}

// Set the page title and include the HTML header:
$page_title = 'Order Confirmation';
include ('includes/header.html');

// get the customer ID from the session
$cid = $_SESSION['customer_id'];

// zero the total before calculating the order cost
$total = 0;


// loop all the items in the cart and add them to the total
foreach($_SESSION['cart'] as $index => $item) {
	$total = $total + $item['price'];
}

require ('../mysqli_connect.php'); // Connect to the database.

// Turn autocommit off:
mysqli_autocommit($dbc, FALSE);

// Add the order to the orders table...
$q = "INSERT INTO orders (customer_id, total) VALUES ($cid, $total)";
$r = mysqli_query($dbc, $q);
if (mysqli_affected_rows($dbc) == 1) {

	// Need the order ID:
	$oid = mysqli_insert_id($dbc);
	
	// Insert the specific order contents into the database...
	
	// Prepare the query:
	$q = "INSERT INTO order_contents (order_id, song_id, quantity, price) VALUES (?, ?, ?, ?)";
	$stmt = mysqli_prepare($dbc, $q);
	mysqli_stmt_bind_param($stmt, 'iiid', $oid, $pid, $qty, $price);
	
	// Execute each query; count the total affected:
	$affected = 0;
	foreach ($_SESSION['cart'] as $pid => $item) {
		$qty = $item['quantity'];
		$price = $item['price'];
		mysqli_stmt_execute($stmt);
		$affected += mysqli_stmt_affected_rows($stmt);
	}

	// Close this prepared statement:
	mysqli_stmt_close($stmt);

	// Report on the success....
	if ($affected == count($_SESSION['cart'])) { // Whohoo!
	
		// Commit the transaction:
		mysqli_commit($dbc);
		
		// Clear the cart:
		unset($_SESSION['cart']);
		
		// Message to the customer:
		echo "<div class=\"alert alert-success\"><p>Thank you for your order. You can now <a href=\"my_tunes.php\">download your tunes from the my tunes page</a>.</p></div>";
		
		// Send emails and do whatever else.
	
	} else { // Rollback and report the problem.
	
		mysqli_rollback($dbc);
		
		echo '<p>Your order could not be processed due to a system error. You will be contacted in order to have the problem fixed. We apologize for the inconvenience.</p>';
		// Send the order information to the administrator.
		
	}

} else { // Rollback and report the problem.

	mysqli_rollback($dbc);

	echo '<p>Your order could not be processed due to a system error. You will be contacted in order to have the problem fixed. We apologize for the inconvenience.</p>';
	
	// Send the order information to the administrator.
	
}

mysqli_close($dbc);

include ('includes/footer.html');
?>