<?php # Script 9.5 - register.php #2
// This script performs an INSERT query to add a record to the customers table.

$page_title = 'Register';
include ('includes/header.html');

// Check for form submission:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

  require ('../mysqli_connect.php'); // Connect to the db.
    
  $errors = array(); // Initialize an error array.
  
  // Check for a first name:
  if (empty($_POST['first_name'])) {
    $errors[] = 'You forgot to enter your first name.';
  } else {
    $fn = mysqli_real_escape_string($dbc, trim($_POST['first_name']));
  }
  
  // Check for a last name:
  if (empty($_POST['last_name'])) {
    $errors[] = 'You forgot to enter your last name.';
  } else {
    $ln = mysqli_real_escape_string($dbc, trim($_POST['last_name']));
  }
  
  // Check for an email address:
  if (empty($_POST['email'])) {
    $errors[] = 'You forgot to enter your email address.';
  } else {
    $e = mysqli_real_escape_string($dbc, trim($_POST['email']));
  }
  
  // Check for a password and match against the confirmed password:
  if (!empty($_POST['pass1'])) {
    if ($_POST['pass1'] != $_POST['pass2']) {
      $errors[] = 'Your password did not match the confirmed password.';
    } else {
      $p = mysqli_real_escape_string($dbc, trim($_POST['pass1']));
    }
  } else {
    $errors[] = 'You forgot to enter your password.';
  }
  
  if (empty($errors)) { // If everything's OK.
  
    // Register the user in the database...
    
    // Make the query:
    $q = "INSERT INTO customers (first_name, last_name, email, pass, registration_date) VALUES ('$fn', '$ln', '$e', SHA1('$p'), NOW() )";   
    $r = @mysqli_query ($dbc, $q); // Run the query.
    if ($r) { // If it ran OK.
    
      // Print a message:
      echo "<h1>Thank you!</h1>
    <p>You are now registered. You can now <a href=\"login.php\" title=\"Login to EZTunes\">login to EZTunes</a></p><p><br /></p>";  
    
    } else { // If it did not run OK.
      
      // Public message:
      echo '<h1>System Error</h1>
      <p class="error">You could not be registered due to a system error. We apologize for any inconvenience.</p>'; 
      
      // Debugging message:
      echo '<p>' . mysqli_error($dbc) . '<br /><br />Query: ' . $q . '</p>';
            
    } // End of if ($r) IF.
    
    mysqli_close($dbc); // Close the database connection.

    // Include the footer and quit the script:
    include ('includes/footer.html'); 
    exit();
    
  } else { // Report the errors.
  
    echo '<h1>Error!</h1>
    <p class="error">The following error(s) occurred:<br />';
    foreach ($errors as $msg) { // Print each error.
      echo " - $msg<br />\n";
    }
    echo '</p><p>Please try again.</p><p><br /></p>';
    
  } // End of if (empty($errors)) IF.
  
  mysqli_close($dbc); // Close the database connection.

} // End of the main Submit conditional.
?>
<form action="register.php" method="post" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Register</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="first_name">First Name</label>  
  <div class="col-md-4">
  <input id="first_name" name="first_name" type="text" placeholder="John" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="last_name">Last Name</label>  
  <div class="col-md-4">
  <input id="last_name" name="last_name" type="text" placeholder="Smith" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="john.smith@example.com" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="pass1">Password</label>
  <div class="col-md-4">
    <input id="pass1" name="pass1" type="password" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="pass2">Password Confirmation</label>
  <div class="col-md-4">
    <input id="pass2" name="pass2" type="password" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-pencil"></span> Register</button>
  </div>
</div>

</fieldset>
</form>
<?php include ('includes/footer.html'); ?>