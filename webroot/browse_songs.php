<?php # Script 19.6 - browse_songs.php
// This page displays the available songs (products).

// Set the page title and include the HTML header:
$page_title = 'Browse Songs';
include ('includes/header.html');

require ('../mysqli_connect.php');

$songsPerPage = 10;

$paginationQuery = "SELECT COUNT(songs.song_id) AS number_of_songs FROM songs WHERE 1";
if(isset($_GET['aid']) && ctype_digit($_GET['aid'])) {
	$paginationQuery = "SELECT COUNT(songs.song_id) AS number_of_songs FROM songs WHERE artist_id=" . $_GET['aid'];
}
$paginationResponse = mysqli_query($dbc, $paginationQuery);
if(mysqli_num_rows($paginationResponse) === 1) {
	$row = mysqli_fetch_assoc($paginationResponse);
	$numberOfPages = ceil($row['number_of_songs'] / $songsPerPage);
	$numberOfSongs = $row['number_of_songs'];
}

if(isset($_GET['page']) && ctype_digit($_GET['page']) && $_GET['page'] <= $numberOfPages) {
	$currentPage = $_GET['page'];
}
else {
	$currentPage = 1;
}

$offset = (($currentPage - 1) * $songsPerPage);

if(isset($_GET['orderby']) && ctype_alnum($_GET['orderby']) && ($_GET['orderby'] === "artist" || $_GET['orderby'] === "song" || $_GET['orderby'] === "price")) {
	switch($_GET['orderby']) {
		case "artist":
			$orderBy = "artists.artist_name";
			break;
		case "song":
			$orderBy = "songs.song_name";
			break;
		case "price":
			$orderBy = "songs.price";
			break;
	}
}
else {
	$orderBy = "artists.artist_name ASC, songs.song_name ASC";
}
 
// Default query for this page:
$q = "SELECT artists.artist_id, artist_name AS artist, song_name, price, song_id, image_name FROM artists, songs WHERE artists.artist_id = songs.artist_id ORDER BY $orderBy LIMIT $offset,$songsPerPage";

// Are we looking at a particular artist?
if (isset($_GET['aid']) && filter_var($_GET['aid'], FILTER_VALIDATE_INT, array('min_range' => 1))  ) {
	// Overwrite the query:
	$q = "SELECT artists.artist_id, artist_name AS artist, song_name, price, song_id, image_name FROM artists, songs WHERE artists.artist_id=songs.artist_id AND songs.artist_id={$_GET['aid']} ORDER BY songs.song_name LIMIT $offset,$songsPerPage";
}
?>

<h1>Browse Tunes</h1>
<p><strong>Page: </strong><?php echo $currentPage; ?> <strong>Number of songs: </strong><?php echo $numberOfSongs; ?> <a href="<?php echo $_SERVER['PHP_SELF']; ?>">Clear All Filters</a></p>

<div class="row">
	<form method="GET" class="form-horizontal">
		<div class="col-md-4">
			<select name="aid" class="form-control" onchange="this.form.submit()">
				<option>Browse By Artist</option>
				<?php // Retrieve all the artists and add to the pull-down menu.
					$aq = "SELECT artist_id, artist_name FROM artists ORDER BY artist_name ASC";		
					$ar = mysqli_query ($dbc, $aq);
					if (mysqli_num_rows($ar) > 0) {
						while ($arow = mysqli_fetch_array ($ar, MYSQLI_NUM)) {
							echo "<option value=\"$arow[0]\"";
							// Check for stickyness:
							if (isset($_POST['artist']) && ($_POST['artist'] == $arow[0]) ) echo ' selected="selected"';
							echo ">$arow[1]</option>\n";
						}
					} else {
						echo '<option>Please add a new artist first.</option>';
					}
					?>
			</select>
		</div>
	</form>
</div>

<?php
	echo "<ul class=\"pagination\">";
	if(($currentPage - 1)>0) {
		if(isset($_GET['orderby']) && ctype_alpha($_GET['orderby'])) {
			echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage - 1) . "&orderby={$_GET['orderby']}\">&laquo;</a></li>";
		}
		elseif(isset($_GET['aid']) && ctype_digit($_GET['aid'])) {
			echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage - 1) . "&aid={$_GET['aid']}\">&laquo;</a></li>";	
		}
		else {
			echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage - 1) . "\">&laquo;</a></li>";
		}
	}
	else {
		echo "<li class=\"disabled\"><a href=\"#\">&laquo;</a></li>";
	}
	for($i=1; $i<=$numberOfPages; $i++) {
		echo "<li";
		if($i == $currentPage) {
	    echo " class=\"active\"";
	  }
		if(isset($_GET['orderby']) && ctype_alpha($_GET['orderby'])) {
	  	echo "><a href=\"$_SERVER[PHP_SELF]?page=$i&orderby={$_GET['orderby']}\">$i</a></li>";
	  }
	  elseif(isset($_GET['aid']) && ctype_digit($_GET['aid'])) {
	  	echo "><a href=\"$_SERVER[PHP_SELF]?page=$i&aid={$_GET['aid']}\">$i</a></li>";	
	  }
	  else {
	  	echo "><a href=\"$_SERVER[PHP_SELF]?page=$i\">$i</a></li>";
	  }
	}
	if($currentPage < $numberOfPages) {
		if(isset($_GET['orderby']) && ctype_alpha($_GET['orderby'])) {
			echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage + 1) . "&orderby={$_GET['orderby']}\">&raquo;</a></li>";
		}
		elseif(isset($_GET['aid']) && ctype_digit($_GET['aid'])) {
			echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage + 1) . "&aid={$_GET['aid']}\">&raquo;</a></li>";
		}
		else {
			echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage + 1) . "\">&raquo;</a></li>";
		}
	}
	else {
		echo "<li class=\"disabled\"><a href=\"#\">&raquo;</a></li>";
	}
	echo "</ul>";
?>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Cover Art</th>
			<?php
				if(isset($_GET['aid'])) {
			?>
					<th>Artist</th>
					<th>Song Name</th>
					<th>Price</th>
					<th>Add To Cart</th>
			<?php
				}
				else {
			?>
				<th><a href="<?php echo $_SERVER['PHP_SELF']; ?>?orderby=artist">Artist</a></th>
				<th><a href="<?php echo $_SERVER['PHP_SELF']; ?>?orderby=song">Song Name</a></th>
				<th><a href="<?php echo $_SERVER['PHP_SELF']; ?>?orderby=price">Price</a></th>
				<th>Add To Cart</th>
			<?php
				}
			?>
		</tr>
	</thead>
<?php
// Display all the songs, linked to URLs:
$r = mysqli_query ($dbc, $q);
$rowcount = mysqli_num_rows($r);
while ($row = mysqli_fetch_array ($r, MYSQLI_ASSOC)) {
	// Display each record:
	echo "\t<tr><td>";
	if ($image = @getimagesize ("../uploads/covers/$row[song_id]")) {
		echo "<img src=\"show_image.php?image=$row[song_id]&name=" . urlencode($row['image_name']) . "\" $image[3] alt=\"{$row['song_name']}\" class=\"coverart thumbnail img-responsive\">";	
	} else {
		echo "No image available."; 
	}
	echo "</td><td><a href=\"browse_songs.php?aid={$row['artist_id']}\">{$row['artist']}</a></td>
		<td><a href=\"view_song.php?pid={$row['song_id']}\">{$row['song_name']}</a></td>
		<td>&pound;{$row['price']}</td>
		<td><a href=\"add_cart.php?pid={$row['song_id']}\" class=\"btn btn-info\"><span class=\"glyphicon glyphicon-plus\"></span></a></td>
	</tr>\n";

} // End of while loop.

echo '</table>';

echo "<ul class=\"pagination\">";
if(($currentPage - 1)>0) {
	if(isset($_GET['orderby']) && ctype_alpha($_GET['orderby'])) {
		echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage - 1) . "&orderby={$_GET['orderby']}\">&laquo;</a></li>";
	}
	elseif(isset($_GET['aid']) && ctype_digit($_GET['aid'])) {
		echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage - 1) . "&aid={$_GET['aid']}\">&laquo;</a></li>";	
	}
	else {
		echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage - 1) . "\">&laquo;</a></li>";
	}
}
else {
	echo "<li class=\"disabled\"><a href=\"#\">&laquo;</a></li>";
}
for($i=1; $i<=$numberOfPages; $i++) {
	echo "<li";
	if($i == $currentPage) {
    echo " class=\"active\"";
  }
	if(isset($_GET['orderby']) && ctype_alpha($_GET['orderby'])) {
  	echo "><a href=\"$_SERVER[PHP_SELF]?page=$i&orderby={$_GET['orderby']}\">$i</a></li>";
  }
  elseif(isset($_GET['aid']) && ctype_digit($_GET['aid'])) {
  	echo "><a href=\"$_SERVER[PHP_SELF]?page=$i&aid={$_GET['aid']}\">$i</a></li>";	
  }
  else {
  	echo "><a href=\"$_SERVER[PHP_SELF]?page=$i\">$i</a></li>";
  }
}
if($currentPage < $numberOfPages) {
	if(isset($_GET['orderby']) && ctype_alpha($_GET['orderby'])) {
		echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage + 1) . "&orderby={$_GET['orderby']}\">&raquo;</a></li>";
	}
	elseif(isset($_GET['aid']) && ctype_digit($_GET['aid'])) {
		echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage + 1) . "&aid={$_GET['aid']}\">&raquo;</a></li>";
	}
	else {
		echo "<li><a href=\"$_SERVER[PHP_SELF]?page=" . ($currentPage + 1) . "\">&raquo;</a></li>";
	}
}
else {
	echo "<li class=\"disabled\"><a href=\"#\">&raquo;</a></li>";
}
echo "<li style=\"margin-left: 10px;\"><a href=\"#top\" class=\"btn btn-primary\"><span class=\"fa fa-arrow-circle-o-up\"></span></a></li>";
echo "</ul>";

mysqli_close($dbc);
include ('includes/footer.html');
?>