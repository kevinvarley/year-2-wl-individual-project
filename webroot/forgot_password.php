<?php # Script 18.10 - forgot_password.php
// This page allows a customer to reset their password, if forgotten.
require ('includes/config.inc.php'); 
$page_title = 'Forgot Your Password';
include ('includes/header.html');

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	require ("../mysqli_connect.php");

	// Assume nothing:
	$uid = FALSE;

	// Validate the email address...
	if (!empty($_POST['email'])) {

		// Check for the existence of that email address...
		$q = 'SELECT customer_id FROM customers WHERE email="'.  mysqli_real_escape_string ($dbc, $_POST['email']) . '"';
		$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));
		
		if (mysqli_num_rows($r) == 1) { // Retrieve the customer ID:
			list($uid) = mysqli_fetch_array ($r, MYSQLI_NUM); 
		} else { // No database match made.
			echo "<div class=\"alert alert-danger\"><p>The submitted email address does not match those on file!</p></div>";
		}
		
	} else { // No email!
		echo '<p class="error">You forgot to enter your email address!</p>';
	} // End of empty($_POST['email']) IF.
	
	if ($uid) { // If everything's OK.

		// Create a new, random password:
		$p = substr ( md5(uniqid(rand(), true)), 3, 10);

		// Update the database:
		$q = "UPDATE customers SET pass=SHA1('$p') WHERE customer_id=$uid LIMIT 1";
		$r = mysqli_query ($dbc, $q) or trigger_error("Query: $q\n<br />MySQL Error: " . mysqli_error($dbc));

		if (mysqli_affected_rows($dbc) == 1) { // If it ran OK.
		
			// Send an email:
			$body = "Your password to log into <whatever site> has been temporarily changed to '$p'. Please log in using this password and this email address. Then you may change your password to something more familiar.";
			mail ($_POST['email'], 'Your temporary password.', $body, 'From: admin@sitename.com');
			
			// Print a message and wrap up:
			echo "<div class=\"alert alert-success\"><p>Your password has been changed. You will receive the new, temporary password at the email address with which you registered. Once you have logged in with this password, you may change it by clicking on the \"Change Password\" link.</p></div><a href=\"index.php\" class=\"btn btn-primary\">Return to EZTunes home</a><a href=\"login.php\" class=\"btn btn-success\" style=\"margin-left: 1rem;\">Login to EZTunes</a>";
			mysqli_close($dbc);
			include ('includes/footer.html');
			exit(); // Stop the script.
			
		} else { // If it did not run OK.
			echo "<div class=\"alert alert-danger\"><p>Your password could not be changed due to a system error. We apologize for any inconvenience.</p></div>"; 
		}

	} else { // Failed the validation test.
		echo "<div class=\"alert alert-info\"><p>Please try again.</p></div>";
	}

	mysqli_close($dbc);

} // End of the main Submit conditional.
?>

<form action="forgot_password.php" method="post" class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Reset Your Password</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="email">Email Address</label>  
  <div class="col-md-4">
  <input id="email" name="email" type="text" placeholder="j.smith@example.com" class="form-control input-md" required="" size="20" maxlength="60" value="<?php if (isset($_POST['email'])) echo $_POST['email']; ?>">
    
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Reset My Password</button>
  </div>
</div>

</fieldset>
</form>


<?php include ('includes/footer.html'); ?>