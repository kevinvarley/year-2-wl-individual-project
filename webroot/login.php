<?php # Script 12.12 - login.php #4
// This page processes the login form submission.
// The script now stores the HTTP_USER_AGENT value for added security.
if(session_id() == '') { // check if a session has already been started
  session_start(); // start session
}

// Check if the form has been submitted:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	// Need two helper files:
	require ('includes/login_functions.inc.php');
	require ('../mysqli_connect.php');
		
	// Check the login:
	list ($check, $data) = check_login($dbc, $_POST['email'], $_POST['pass']);
	
	if ($check) { // OK!
		
		$_SESSION['customer_id'] = $data['customer_id'];
		$_SESSION['first_name'] = $data['first_name'];
		$_SESSION['last_name'] = $data['last_name'];
		
		// Store the HTTP_USER_AGENT:
		$_SESSION['agent'] = md5($_SERVER['HTTP_USER_AGENT']);

		if (isset($_POST['redirectto']) && ctype_alpha($_POST['redirectto']) && $_POST['redirectto'] === "cart" && isset($_POST['pid']) && is_numeric($_POST['pid'])) {
			// redirect the user & add the requested song to cart
			redirect_customer('add_cart.php?pid=' . $_POST['pid']);
		}
		else {
			// redirct to default loggedin landing page
			redirect_customer('loggedin.php');
		}
			
	} else { // Unsuccessful!

		// Assign $data to $errors for login_page.inc.php:
		$errors = $data;

	}
		
	mysqli_close($dbc); // Close the database connection.

} // End of the main submit conditional.

// Create the page:
include ('includes/login_page.inc.php');
?>