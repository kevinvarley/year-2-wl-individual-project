<?php # Script 19.9 - add_cart.php
// This page adds songs to the shopping cart.

session_start();

if (!isset($_SESSION['customer_id'])) {
	header('Location: login.php?redirectto=cart&' . $_SERVER['QUERY_STRING']);
	exit();
}

// Set the page title and include the HTML header:
$page_title = 'Add to Cart';
include ('includes/header.html');

if (isset ($_GET['pid']) && filter_var($_GET['pid'], FILTER_VALIDATE_INT, array('min_range' => 1))  ) { // Check for a song ID.
	$pid = $_GET['pid'];

	// Check if the cart already contains one of these songs;
	// If so, increment the quantity:
	if (isset($_SESSION['cart'][$pid])) {

		//$_SESSION['cart'][$pid]['quantity']++; // Add another.

		// Display a message:
		echo "<div class=\"alert alert-danger\"><p>This song is already in your <a href=\"view_cart.php\">shopping cart</a>!</p></div>";
		
	} else { // New product to the cart.

		// Get the song's price from the database:
		require ('../mysqli_connect.php'); // Connect to the database.
		$q = "SELECT price FROM songs WHERE song_id=$pid";
		$r = mysqli_query ($dbc, $q);		
		if (mysqli_num_rows($r) == 1) { // Valid song ID.
	
			// Fetch the information.
			list($price) = mysqli_fetch_array ($r, MYSQLI_NUM);
			
			// Add to the cart:
			$_SESSION['cart'][$pid] = array ('quantity' => 1, 'price' => $price);

			// Display a message:
			echo "<div class=\"alert alert-success\"><p>The song has been added to your <a href=\"view_cart.php\">shopping cart</a>.</p></div>";

		} else { // Not a valid song ID.
			echo "<div class=\"alert alert-danger\"><p>This page has been accessed in error! <a href=\"index.php\">Return to EZTunes home</a>.</p></div>";
		}
		
		mysqli_close($dbc);

	} // End of isset($_SESSION['cart'][$pid] conditional.

} else { // No song ID.
	echo "<div class=\"alert alert-danger\"><p>This page has been accessed in error! <a href=\"index.php\">Return to EZTunes home</a>.</p></div>";
}

include ('includes/footer.html');
?>