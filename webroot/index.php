<?php # Script 19.5 - index.php
// This is the main page for the site.
// Set the page title and include the HTML header:
$page_title = 'Home';
include ('includes/header.html');
?>

<h1>Why EZTunes?</h1>

<hr class="featurette-divider">

<div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">Massive library <span class="text-muted"> - over 1 million tunes</span></h2>
    <p class="lead">With over 1 million songs to choose from on EZTunes you'll be sure to find something you love! Don't have a tune you want to download? <a href="email.php">Tell us to add a song to EZTunes</a>.</p>
  </div>
  <div class="col-md-5">
    <span class="fa fa-music"></span>
  </div>
</div>

<hr class="featurette-divider">

<div class="row featurette">
  <div class="col-md-5">
    <span class="fa fa-unlock"></span>
  </div>
  <div class="col-md-7">
    <h2 class="featurette-heading">DRM free <span class="text-muted"> - any tune, any device, any time</span></h2>
    <p class="lead">All of the songs on EZTunes are DRM free which means you can play your music anywhere. Want a copy on your MP3 player, iPod, Smart TV and in the car? No problem! You'll be saying goodbye to iTunes in no time...</p>
  </div>
</div>

<hr class="featurette-divider">

<div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">Cloud backup <span class="text-muted"> - never lose your music again</span></h2>
    <p class="lead">Lost your tunes? No problem! You can <strong>always</strong> login to EZTunes and download all your tunes from your personal library. No more download limits!</p>
  </div>
  <div class="col-md-5">
    <span class="fa fa-cloud-download"></span>
  </div>
</div>

<hr class="featurette-divider">

<div class="row featurette">
  <div class="col-md-5">
    <span class="fa fa-mobile"></span>
  </div>
  <div class="col-md-7">
    <h2 class="featurette-heading">On the go <span class="text-muted"> - sync tunes, create playlists</span></h2>
    <p class="lead">All of EZTunes services are available on your mobile devices. Access EZTunes in your mobile browser or download our app for your device. Buy music, create playlist and sync music from your personal library on-the-go.</p>
  </div>
</div>

<hr class="featurette-divider">

<div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">Still not convinced?</h2>
    <p class="lead">If you're still not convinced that EZTunes is a quick, easy and legal way to download your music - <a href="#">check out our customer reviews</a></p>
  </div>
  <div class="col-md-5">
    <span class="fa fa-users"></span>
  </div>
</div>


<hr class="featurette-divider">

<?php include ('includes/footer.html'); ?>