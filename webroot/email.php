<?php # Script 11.1 - email.php
$page_title = 'Contact Us';
include ('includes/header.html');

// Check for form submission:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	// Minimal form validation:
	if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['comments']) ) {
		if(ctype_alpha(str_replace(' ', '', $_POST['name']))) {
			if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
				// Create the body:
				$body = "Name: {$_POST['name']}\n\nComments: {$_POST['comments']}";

				// Make it no longer than 70 characters long:
				$body = wordwrap($body, 70);
			
				// Send the email:
				mail('your_email@example.com', 'Contact Form Submission', $body, "From: {$_POST['email']}");

				// Print a message:
				echo "<div class=\"alert alert-success\"><p>Thanks for contacting EZTunes. One of our agents will be with your shortly...</p></div>";
				
				// Clear $_POST (so that the form's not sticky):
				$_POST = array();
			}
			else {
				echo "<div class=\"alert alert-danger\"><p>Invalid email address entered.</p></div>";
			}
		}
		else {
			echo "<div class=\"alert alert-danger\"><p>Invalid name entered. Only alphabet characters and spaces are allowed!</p></div>";
		}
	
	} else {
		echo "<div class=\"alert alert-danger\"><p>Please fill out the form completely.</p></div>";
	}
	
} // End of main isset() IF.

// Create the HTML form:
?>
<form action="email.php" method="post" class="form-horizontal">
	<fieldset>

	<!-- Form Name -->
	<legend>Contact Us</legend>

	<!-- Text input-->
	<div class="form-group">
	  <label class="col-md-4 control-label" for="name">Name</label>  
	  <div class="col-md-4">
	  <input id="name" name="name" type="text" placeholder="John Smith" class="form-control input-md" required="">
	    
	  </div>
	</div>

	<!-- Text input-->
	<div class="form-group">
	  <label class="col-md-4 control-label" for="email">Email</label>  
	  <div class="col-md-4">
	  <input id="email" name="email" type="email" placeholder="j.smith@example.com" class="form-control input-md" required="">
	    
	  </div>
	</div>

	<!-- Textarea -->
	<div class="form-group">
	  <label class="col-md-4 control-label" for="comments">Message</label>
	  <div class="col-md-4">                     
	    <textarea class="form-control" id="comments" name="comments">Enter your message here...</textarea>
	  </div>
	</div>

	<!-- Button -->
	<div class="form-group">
	  <label class="col-md-4 control-label" for="submit"></label>
	  <div class="col-md-4">
	    <button id="submit" name="submit" type="submit" class="btn btn-primary btn-block"><span class="fa fa-envelope"></span> Send</button>
	  </div>
	</div>

	</fieldset>
</form>
</body>
</html>