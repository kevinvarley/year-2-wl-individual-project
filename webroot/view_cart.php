<?php # Script 19.10 - view_cart.php
// This page displays the contents of the shopping cart.
// This page also lets the user update the contents of the cart.

// Set the page title and include the HTML header:
session_start();

if (!isset($_SESSION['customer_id'])) {
	header('Location: login.php');
	exit();
}

$page_title = 'Shopping Cart';
include ('includes/header.html');

// Check if the form has been submitted (to update the cart):
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	foreach($_POST['song_ids'] as $index => $value) {
		unset ($_SESSION['cart'][$value]);
	}

	if(sizeof($_SESSION['cart']) === 0) {
		unset($_SESSION['cart']);
	}

	// Change any quantities:
	/*foreach ($_POST['qty'] as $k => $v) {

		// Must be integers!
		$pid = (int) $k;
		$qty = (int) $v;
		
		if ( $qty == 0 ) { // Delete.
			unset ($_SESSION['cart'][$pid]);
		} elseif ( $qty > 0 ) { // Change quantity.
			$_SESSION['cart'][$pid]['quantity'] = $qty;
		}
		
	} */ // End of FOREACH.
	
} // End of SUBMITTED IF.

// Display the cart if it's not empty...
if (!empty($_SESSION['cart'])) {

	// Retrieve all of the information for the songs in the cart:
	require ('../mysqli_connect.php'); // Connect to the database.
	$q = "SELECT song_id, artist_name AS artist, song_name FROM artists, songs WHERE artists.artist_id = songs.artist_id AND songs.song_id IN (";
	foreach ($_SESSION['cart'] as $pid => $value) {
		$q .= $pid . ',';
	}
	$q = substr($q, 0, -1) . ') ORDER BY artists.artist_name ASC';
	$r = mysqli_query ($dbc, $q);
	
	// Create a form and a table:
	echo "<div class=\"row\"><form action=\"view_cart.php\" method=\"post\">
	<table class=\"table table-striped\">
	<tr>
		<th>Artist</th>
		<th>Song Name</th>
		<th>Price</th>
		<th>Remove?</th>
	</tr>
	";

	// song each item...
	$total = 0; // Total cost of the order.
	while ($row = mysqli_fetch_array ($r, MYSQLI_ASSOC)) {
	
		// Calculate the total
		$total += $_SESSION['cart'][$row['song_id']]['price'];
		
		// song the row:
		echo "\t<tr>
		<td>{$row['artist']}</td>
		<td>{$row['song_name']}</td>
		<td>&pound;{$_SESSION['cart'][$row['song_id']]['price']}</td>
		<td><input type=\"checkbox\" name=\"song_ids[]\" value=\"$row[song_id]\"></td>
		</tr>";
	
	} // End of the WHILE loop.

	mysqli_close($dbc); // Close the database connection.

	// song the total, close the table, and the form:
	echo '<tr>
		<td colspan="3" style="text-align: right;"><b>Total:</b></td>
		<td>&pound;' . number_format ($total, 2) . '</td>
	</tr>
	</table>
	</div>
	<div class="row">
		<input type="submit" name="submit" value="Remove Selected" class="btn btn-danger pull-right">
		<a href="checkout.php" class="btn btn-success pull-right" style="margin-right: 1rem;"><span class="fa fa-credit-card"></span> Checkout</a>
	</div>
	</form>';

} else {
		echo "<div class=\"alert alert-info\"><p>Your cart is currently empty. Do you want to <a href=\"browse_songs.php\">browse songs</a>?</p></div>";
}

include ('includes/footer.html');
?>